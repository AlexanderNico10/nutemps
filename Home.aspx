﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" Title="NuTemps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="text-align: center">
        Please login.</p>
    <table class="style1" cellpadding="5">
        <tr>
            <td style="text-align: center">
                <asp:HyperLink ID="hypStudent" runat="server" NavigateUrl="~/StudentLogin.aspx" 
                    style="text-align: center">Login 
                as a student</asp:HyperLink>
            </td>
            <td style="text-align: center">
                <asp:HyperLink ID="hypCompany" runat="server" NavigateUrl="~/CompanyLogin.aspx">Login 
                as a company</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Image ID="imgSLog" runat="server" 
                    ImageUrl="~/assets/Images/grad_transparent.png.cf.png" Height="200px" 
                    Width="400px" />
            </td>
            <td style="text-align: center">
                <asp:Image ID="imgCLog" runat="server" 
                    ImageUrl="~/assets/Images/grtplc.jpg" Height="200px" Width="400px" />
            </td>
        </tr>
    </table>
</asp:Content>

