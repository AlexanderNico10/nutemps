﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Image imgK = Master.FindControl("imgK") as Image;
        Label lblFoot = Master.FindControl("lblFoot") as Label;
        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        imgK.Visible = false;
        lblFoot.Visible = false;

        if (Session["companyID"] != null)
        {
            btnLogout.Visible = true;
        }
        else if (Session["studcode"] != null)
        {
            btnLogout.Visible = true;
        }
    }
}