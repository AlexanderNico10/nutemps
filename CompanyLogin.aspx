﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CompanyLogin.aspx.cs" Inherits="CompanyLogin" Title="Company Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Please Login</p>
    <table class="style1">
        <tr>
            <td style="width:50%">
                Company Login Name:</td>
                                    <td style="width:50%">
                                        <asp:TextBox ID="txtCompanyLogin" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Password:</td>
                                    <td>
                                        <asp:TextBox ID="txtCompanyPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblError" runat="server" 
    ForeColor="Red" style="text-align: center"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;
                            <table class="style1">
                                <tr>
                                    <td style="width:50%">
                                        <asp:Button ID="btnCompanyLogin" runat="server" Text="Login as a Company" 
                                            onclick="btnCompanyLogin_Click" />
                                    </td>
                                    <td style="width:50%">
                                        Not yet a member?                 <asp:HyperLink ID="hypRegisterCompany" runat="server" 
                    NavigateUrl="~/CompanyRegister.aspx">Register your company here</asp:HyperLink>
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Content>

