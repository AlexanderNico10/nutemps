DROP Table Student
DROP Table Company
DROP Table Job

CREATE TABLE Student (
	StudCode	varchar(20)	NOT NULL	PRIMARY KEY,	
	FirstName 	varchar(30) NOT NULL,
	LastName 	varchar(30) NOT NULL,
	Telephone	varchar(13),
	Course 		varchar(30),
	Pword 		varchar(20) NOT NULL,
    JobSkill	varchar(100),
	Experience	varchar(255)
)

insert into Student(StudCode, FirstName, LastName, Telephone, Course, Pword, JobSkill, Experience)
values	('1010', 'John', 'Smith', '8806237890123', 'Electrical Engineering', 'JohnsPassword', 'Problem Solving', '1 year'),
		('2020', 'Serena', 'Williams', '9107128901234', 'Sports Science', 'WilliamsPassword', 'Interpersonal Skills', '2 years'),
		('3030', 'Abedi', 'Pele', '7611159012345', 'Medical Nanotechnology', 'PelesPassword', 'A.I. Programming', '3 years'),
		('4040', 'Jack', 'Donaghy', '7506120123456', 'Business Studies', 'DonaghysPassword', 'Public Relations', '4 years'),
		('5050', 'Shaka', 'Zulu', '8605304572789', 'Juris Doctor', 'ShakasPassword', 'Criminal Law', '3 years');

CREATE TABLE Company (
	CompanyID		INTEGER IDENTITY(1111,11) PRIMARY KEY,	
	CompName 		varchar(100) NOT NULL,
	BusinessType	varchar(30) NOT NULL,
	ContactPerson 	varchar(20) NOT NULL,
	Telephone		varchar(50),
	Address			varchar(255),
	EmailAddress	varchar(50),
	Pword 			varchar(20) NOT NULL
)

insert into Company(CompName, BusinessType, ContactPerson, Telephone, Address, EmailAddress, Pword)
values	('Kim LTD', 'Limited', 'Kim Jung Un', '1006120123456', 'Kims Address, Box 270, Earth', 'Skill21@kim.com', 'KimsPassword'),
		('Torsten Corp', 'Corporation', 'Toronto Headquarters', '2006120123456', 'TCor, Box 290, Earth', 'Skill22@torsten.com', 'TorstensPassword'),
		('Nuri INT', 'International', 'Alexander Merkel', '3006120123456', 'NInt, Box 310, Earth', 'Skill23@nuri.com', 'NurisPassword'),
		('Navas PTY', 'Private', 'Seville President', '7506120123456', 'JPTY, Box 330, Earth', 'Skill24@navas.com', 'NavasPassword');

CREATE TABLE Job (
	JobID			INTEGER IDENTITY(1234,12) PRIMARY KEY ,	
	JobType 		varchar(30) NOT NULL,
	Description		varchar(250) NOT NULL,
	StartDate 		date NOT NULL,
	Time			time NOT NULL,
	CompanyID		INTEGER NOT NULL FOREIGN KEY REFERENCES Company(CompanyID),
	PositionFilled	BIT NOT NULL DEFAULT 0,
	JobStatus 		varchar(20)
)

insert into Job(JobType, Description, StartDate, Time, CompanyID, PositionFilled, JobStatus)
values	('Project Coordinator', 'Coordinating activities, liaising with clients and making certain that client needs are met.', '2017-01-31', '07:00', 1111, 0, 'Vacant'),
		('Academic Content Manager', 'Manage the creation of academic material and interview prospective academic authors and quality analysts', '2017-02-22', '09:00', 1111, 1, 'Filled'),
		('Service Engineer', 'Ensure compliance to Safety and Quality Assurance', '2017-02-01', '09:00', 1122, 0, 'Vacant'),
		('GCC Maintenance Engineer', 'Electrical and Mechanical GCC maintenance with experience in project engineering', '2017-03-02', '08:00', 1122, 1, 'Filled'),
		('Medical Practitioner', 'Clinical management of participants, contraception management and laboratory result management', '2017-02-20', '09:00', 1133, 1, 'Filled'),
		('Labarotory Technologist', 'Perform and analyse reults of complex scientific tests using sophisticated procedures and equipment', '2017-02-22', '08:00', 1133, 0, 'Vacant'),
		('Chartered Accountant', 'Management of financial systems and budgets, financial auditing and providing financial advice', '2017-03-01', '08:00', 1144, 0, 'Vacant'),
		('Financial Analyst', 'Improving financial status by result analysis, monitoring variances and identifying trends', '2017-01-29', '09:00', 1144, 1, 'Filled');
		
select * from Student
select * from Company
select * from Job