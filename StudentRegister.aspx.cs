﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class StudentRegister : System.Web.UI.Page
{
    SqlConnection SqlConn;
    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Check for postback - only retrieve the data on first
            // entering the page.
            MakeConnection();

            string SqlString = "Select * from Company";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Company");
            SqlDA.Fill(DS);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            MakeConnection();
            SqlConn.Open();
            string sqlString = "insert into student(StudCode, FirstName, LastName, Telephone, Course, Pword, JobSkill, Experience)" +
                "values(@StudCode, @FirstName, @LastName, @Telephone, @Course, @Pword,@JobSkill,@Experience)";
            SqlCommand SqlCmd = new SqlCommand(sqlString, SqlConn);
            // Give parameters values
            SqlCmd.Parameters.AddWithValue("@StudCode", txtCode.Text);
            SqlCmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            SqlCmd.Parameters.AddWithValue("@pword", txtPassword.Text);
            SqlCmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            SqlCmd.Parameters.AddWithValue("@Telephone", txtTelephone.Text);
            SqlCmd.Parameters.AddWithValue("@Course", txtCourse.Text);
            SqlCmd.Parameters.AddWithValue("@JobSkill", txtSkill.Text);
            SqlCmd.Parameters.AddWithValue("@Experience", txtExperience.Text);
            SqlCmd.ExecuteNonQuery();
            Response.Redirect("StudentLogin.aspx");
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
        finally  // Connection should always be closed, so add to finally section
        {
            SqlConn.Close();
        }
    }
}
