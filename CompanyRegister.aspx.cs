﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class CompanyRegister : System.Web.UI.Page
{
    SqlConnection SqlConn;
    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Check for postback - only retrieve the data on first
            // entering the page.
            MakeConnection();

            string SqlString = "Select * from Company";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Company");
            SqlDA.Fill(DS);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;    
        }
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            MakeConnection();
            SqlConn.Open();
            string sqlString = "set identity_insert company on insert into Company(BusinessType, compname, pword, ContactPerson, Telephone, EmailAddress, CompanyID, Address)"+
                "values(@BusinessType, @compname, @pword, @ContactPerson, @Telephone, @EmailAddress, @CompanyID, @address) set identity_insert company off";
            SqlCommand SqlCmd = new SqlCommand(sqlString, SqlConn);
            // Give parameters values
            SqlCmd.Parameters.AddWithValue("@BusinessType", txtType.Text);
            SqlCmd.Parameters.AddWithValue("@compname", txtName.Text);
            SqlCmd.Parameters.AddWithValue("@pword", txtPassword.Text);
            SqlCmd.Parameters.AddWithValue("@ContactPerson", txtPerson.Text);
            SqlCmd.Parameters.AddWithValue("@Telephone", txtTelephone.Text);
            SqlCmd.Parameters.AddWithValue("@EmailAddress", txtEmail.Text);
            SqlCmd.Parameters.AddWithValue("@CompanyID", txtID.Text);
            SqlCmd.Parameters.AddWithValue("@address", txtAddress.Text);
            SqlCmd.ExecuteNonQuery();
            Response.Redirect("CompanyLogin.aspx");
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
        finally  // Connection should always be closed, so add to finally section
        {
            SqlConn.Close();
        }
    }
}
