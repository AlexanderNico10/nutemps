﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SearchStudents.aspx.cs" Inherits="SearchStudents" Title="Search Student Criteria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="assets/js/jquery-1.11.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#ctl00_ContentPlaceHolder1_txtCourse').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: 'WebService.asmx/GetCourse',
                            method: 'post',
                            contentType: 'application/json;charset-utf-8',
                            data: JSON.stringify({ term: request.term }),
                            dataType: 'json',
                            success: function (data) {
                                response(data.d)
                            },
                            error: function (err) {
                                alert(err);
                            }
                        });
                    }
                });
            });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_txtSkill').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: 'WebService.asmx/GetSkill',
                        method: 'post',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ term: request.term }),
                        dataType: 'json',
                        success: function (data) {
                            response(data.d)
                        },
                        error: function (err) {
                            alert(err);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Search Students according to the following criteria</p>
    
    <asp:Panel ID="pnlCourse" runat="server" DefaultButton="btnCourse">
    <table class="style1" cellpadding="5">
        <tr>
            <td style="width:33%">
                Course</td>
            <td style="width:33%">
                <asp:TextBox ID="txtCourse" runat="server"></asp:TextBox>
            </td>
            <td style="width:33%">
                <asp:Button ID="btnCourse" runat="server" onclick="btnCourse_Click" 
                    Text="Search Course" Width="157px" />
            </td>
        </tr>
    </table>
    </asp:Panel>

    <asp:Panel ID="pnlSkill" runat="server" DefaultButton="btnSkill">
    <table class="style1" cellpadding="5">
        <tr>
            <td style="width:33%">
                Skill Level</td>
            <td style="width:33%">
                <asp:TextBox ID="txtSkill" runat="server"></asp:TextBox>
            </td>
            <td style="width:33%">
                <asp:Button ID="btnSkill" runat="server" onclick="btnSkill_Click" 
                    style="height: 26px" Text="Search Skill Level" Width="157px" />
            </td>
        </tr>
    </table>
    </asp:Panel>
    <br />
    <asp:GridView ID="grdStudents" runat="server" HorizontalAlign="Center" 
        CellPadding="4" EnableModelValidation="True" ForeColor="#333333" 
        GridLines="None">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    <br />
    <p>
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </p>
</asp:Content>

