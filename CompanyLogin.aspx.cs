﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class CompanyLogin : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["companyID"] != null)
        {
            Response.Redirect("CompanyMenu.aspx");
        }
        try
        {
            // Check for postback - only retrieve the data on first
            // entering the page.

            lblError.Visible = false;

            MakeConnection();

            string SqlString = "Select * from Company";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Company");
            SqlDA.Fill(DS);

            lblError.Text = Convert.ToString(DS.Tables[0].Rows[0]["compname"]);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
    }

    protected void btnCompanyLogin_Click(object sender, EventArgs e)
    {
        try
        {
            MakeConnection();
            SqlConn.Open();
            string Sql = "Select * from company where compname='" + txtCompanyLogin.Text + "'";
            SqlCommand SqlCmd = new SqlCommand(Sql, SqlConn);
            SqlDataReader sqlDR = SqlCmd.ExecuteReader();

            while (sqlDR.Read())
            {
                string compName = (string)sqlDR["compname"];
                int companyID = (int)sqlDR["companyID"];
                string pWord = (string)sqlDR["PWord"];
                if (txtCompanyPassword.Text == pWord)
                {
                    Session["compname"] = compName;
                    Session["companyID"] = companyID;
                    Response.Redirect("CompanyMenu.aspx");
                }
            }

            lblError.Text = "Invalid Login Details";
            lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
        finally
        {
            SqlConn.Close();
        }
    }
}