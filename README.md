NuTemps Career Portal
===

Licensing Information: READ LICENSE
---

Project source: https://AlexanderNico10@bitbucket.org/AlexanderNico10/nutemps.git
---

File List
---

```
About.aspx
About.aspx.cs
CompanyLogin.aspx
CompanyLogin.aspx.cs
CompanyMenu.aspx
CompanyMenu.aspx.cs
CompanyRegister.aspx
CompanyRegister.aspx.cs
CompanyUpdate.aspx
CompanyUpdate.aspx.cs
Home.aspx
Home.aspx.cs
MasterPage.master
MasterPage.master.cs
PostJob.aspx
PostJob.aspx.cs
SearchCurrentJobs.aspx
SearchCurrentJobs.aspx.cs
SearchStudents.aspx
SearchStudents.aspx.cs
ShowJobs.aspx
ShowJobs.aspx.cs
StudentEditRegister.aspx
StudentEditRegister.aspx.cs
StudentLogin.aspx
StudentLogin.aspx.cs
StudentMenu.aspx
StudentMenu.aspx.cs
StudentRegister.aspx
StudentRegister.aspx.cs
Web.config
Web.sitemap
WebService.asmx
favicon.ico
LICENSE
README.md
./App_Code
./App_Data
./assets
```

```
/App_Code

WebService.cs
```

```
/App_Data

NuTemps.sln
NuTemps.suo
```

```
/assets

nutemps1.sql
```

```
/assets/css

jquery-ui.css
stylesheet.css
```

```
/assets/js

jquery-1.11.2.js
jquery-ui.min.js
```

How to run site
---

Run the Microsoft SQL Server Query file to setup the needed tables
in the SQL Server database.  Open the root folder in Visual Studio and
configure the database connection string in the web.config file,
then view Home.aspx on your web browser.