﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class StudentLogin : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check for postback - only retrieve the data on first
        // entering the page.
        if (Session["studcode"] != null)
        {
            Response.Redirect("StudentMenu.aspx");
        }
        if (!Page.IsPostBack)
        {
            try
            {
                MakeConnection();

                string SqlUser = "SELECT * From Student";
                SqlDataAdapter SqlDA = new SqlDataAdapter(SqlUser, SqlConn);
                DataSet DS = new DataSet("Student");
                SqlDA.Fill(DS);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
                lblError.Visible = true;
            }
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            MakeConnection();
            SqlConn.Open();
            string Sql = "Select * from student where studcode='" + txtLogin.Text + "'";
            SqlCommand SqlCmd = new SqlCommand(Sql, SqlConn);
            SqlDataReader sqlDR = SqlCmd.ExecuteReader();

            while (sqlDR.Read())
            {
                string Name = (string)sqlDR["firstname"];
                string studCode = (string)sqlDR["studCode"];
                string pWord = (string)sqlDR["PWord"];
                if (txtPassword.Text == pWord)
                {
                    Session["name"] = Name;
                    Session["studcode"] = studCode;
                    Response.Redirect("StudentMenu.aspx");
                }
            }

            lblError.Text = "Invalid Login Details";
            lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
        finally
        {
            SqlConn.Close();
        }
    }
}
