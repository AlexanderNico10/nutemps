﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class StudentRegister : System.Web.UI.Page
{
    SqlConnection SqlConn;
    
    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["studcode"] == null)
        {
            Response.Redirect("StudentLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;

        if (!IsPostBack)
        {
            try
            {
                // Check for postback - only retrieve the data on first
                // entering the page.
                MakeConnection();

                string SqlString = "Select * from Student";
                SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
                DataSet DS = new DataSet("Student");
                SqlDA.Fill(DS);

                SqlConn.Open();
                string Sql = "Select * from student where studcode='" + Convert.ToString(Session["studcode"]) + "'";
                SqlCommand SqlCmd = new SqlCommand(Sql, SqlConn);
                SqlDataReader sqlDR = SqlCmd.ExecuteReader();

                while (sqlDR.Read())
                {
                    txtCode.Text = (sqlDR["studcode"].ToString());
                    txtFirstName.Text = (sqlDR["FirstName"].ToString());
                    txtLastName.Text = (sqlDR["LastName"].ToString());
                    txtTelephone.Text = (sqlDR["Telephone"].ToString());
                    txtCourse.Text = (sqlDR["Course"].ToString());
                    txtPassword.Text = (sqlDR["Pword"].ToString());
                    txtSkill.Text = (sqlDR["JobSkill"].ToString());
                    txtExperience.Text = (sqlDR["Experience"].ToString());
                }

            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
            }
            finally
            {
                SqlConn.Close();
            }
        }
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            MakeConnection();
            SqlConn.Open();
            string sqlString = "update student set " +
                "FirstName=@FirstName, LastName=@LastName, " +
                "Telephone=@Telephone, Course=@Course, Pword=@Pword,JobSkill=@JobSkill,Experience=@Experience " +
                "where StudCode='" + txtCode.Text + "'";
            SqlCommand SqlCmd = new SqlCommand(sqlString, SqlConn);
            // Give parameters values
            SqlCmd.Parameters.AddWithValue("@StudCode", txtCode.Text);            
            SqlCmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);            
            SqlCmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            SqlCmd.Parameters.AddWithValue("@Telephone", txtTelephone.Text);
            SqlCmd.Parameters.AddWithValue("@Course", txtCourse.Text);
            SqlCmd.Parameters.AddWithValue("@pword", txtPassword.Text);
            SqlCmd.Parameters.AddWithValue("@JobSkill", txtSkill.Text);
            SqlCmd.Parameters.AddWithValue("@Experience", txtExperience.Text);
            SqlCmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
            lblError.Visible = true;
        }
        finally  // Connection should always be closed, so add to finally section
        {
            SqlConn.Close();
        }
    }
}
