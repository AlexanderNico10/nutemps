﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class SearchCurrentJobs : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["studcode"] == null)
        {
            Response.Redirect("StudentLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;
        // Check for postback - only retrieve the data on first
        // entering the page.
        if (!Page.IsPostBack)
        {
            try
            {
                MakeConnection();

                string SqlUser = "SELECT * From Job";
                SqlDataAdapter SqlDA = new SqlDataAdapter(SqlUser, SqlConn);
                DataSet DS = new DataSet("Job");
                SqlDA.Fill(DS);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
            }
        }
    }
    protected void btnType_Click(object sender, EventArgs e)
    {
        lblError.Text = "";

        try
        {
            txtDescript.Text = "";

            MakeConnection();

            string SqlString = "Select * from job where JobType like '%" +
                    txtType.Text + "%'";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Job");
            SqlDA.Fill(DS);

            // Check of any records were returned by the query
            if (DS.Tables[0].Rows.Count > 0)
            {
                // Display the returned record's values in the textboxes
                grdJobs.DataSource = DS;
                grdJobs.DataBind();

            }
            else
            {
                grdJobs.DataSource = DS;
                grdJobs.DataBind();
                // If no data were returned (the record not found),
                // clear the textboxes and display message
                lblError.Text = "No data was found.";
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
        }

    }
    protected void btnDescription_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        try
        {
            txtType.Text = "";

            MakeConnection();

            string SqlString = "Select * from job where Description like '%" +
                    txtDescript.Text + "%'";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Job");
            SqlDA.Fill(DS);

            // Check of any records were returned by the query
            if (DS.Tables[0].Rows.Count > 0)
            {
                // Display the returned record's values in the textboxes
                grdJobs.DataSource = DS;
                grdJobs.DataBind();

            }
            else
            {
                grdJobs.DataSource = DS;
                grdJobs.DataBind();
                // If no data were returned (the record not found),
                // clear the textboxes and display message
                lblError.Text = "No data was found.";
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
        }

    }
}