﻿<%@ Page Title="Student Login" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StudentLogin.aspx.cs" Inherits="StudentLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style5
        {
            width: 379px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p title="Student Login">
    Please Login</p>
<table class="style1" align="center">
    <tr title="Student Login">
        <td style="width:50%">
            Student Code:</td>
        <td style="width:50%">
            <asp:TextBox ID="txtLogin" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Password:</td>
        <td>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
    </tr>
</table>
<asp:Label ID="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
<br />
<table class="style1">
    <tr>
        <td style="width:50%">
            <asp:Button ID="btnLogin" runat="server" onclick="btnLogin_Click" 
                Text="Login as a student" />
        </td>
        <td style="width:50%">
            Not yet a member?
            <asp:HyperLink ID="hypRegisterStudent" runat="server" 
                NavigateUrl="~/StudentRegister.aspx">Register as a student here</asp:HyperLink>
        </td>
    </tr>
</table>
</asp:Content>

