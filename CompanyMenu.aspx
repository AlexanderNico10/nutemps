﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CompanyMenu.aspx.cs" Inherits="CompanyMenu" Title="Company Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    You are now logged in as
    <asp:Label ID="lblLogged" runat="server"></asp:Label>
    <br />
    <br />
    <asp:GridView ID="grvJobs" runat="server" GridLines="None" 
        HorizontalAlign="Center" CellPadding="4" EnableModelValidation="True" 
        ForeColor="#333333">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    <br />
    <asp:Label ID="lblError" runat="server"></asp:Label>
    <br />
    <table class="style1" cellpadding="5">
        <tr>
            <td style="width:25%">
                <asp:HyperLink ID="hypPostJob" runat="server" NavigateUrl="~/PostJob.aspx">Post Job Details</asp:HyperLink>
            </td>
            <td style="width:25%">
                <asp:HyperLink ID="hypShowJob" runat="server" 
                    NavigateUrl="~/ShowJobs.aspx">Show Job Details</asp:HyperLink>
            </td>
            <td style="width:25%">
                <asp:HyperLink ID="hypCompany" runat="server" 
                    NavigateUrl="~/CompanyUpdate.aspx">Update Company Details</asp:HyperLink>
            </td>
            <td style="width:25%">
                <asp:HyperLink ID="hypSearch" runat="server" 
                    NavigateUrl="~/SearchStudents.aspx">Search Student criteria</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="width:25%">
                <asp:Image ID="imgPost" runat="server" Height="170px" 
                    ImageUrl="~/assets/Images/11950012851926662317document.svg.med.png.cf.png" 
                    Width="200px" />
            </td>
            <td style="width:25%">
                <asp:Image ID="imgShow" runat="server" Height="150px" 
                    ImageUrl="~/assets/Images/jobs.png.cf.png" Width="200px" />
            </td>
            <td style="width:25%">
                <asp:Image ID="imgUpdate" runat="server" Height="170px" 
                    ImageUrl="~/assets/Images/available-updates-xxl.png.cf.png" Width="200px" />
            </td>
            <td style="width:25%">
                <asp:Image ID="imgSearch" runat="server" Height="170px" 
                    ImageUrl="~/assets/Images/magnifying_glass.png.cf.png" Width="200px" />
            </td>
        </tr>
    </table>
</asp:Content>

