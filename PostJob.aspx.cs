﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class PostJob : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["companyID"] == null)
        {
            Response.Redirect("CompanyLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;

        Image imgk = Master.FindControl("imgK") as Image;
        imgk.Visible = false;

        Label lblFoot = Master.FindControl("lblFoot") as Label;
        lblFoot.Visible = false;

        txtCompanyID.Text = Session["CompanyID"].ToString();
    }
    protected void btnPost_Click(object sender, EventArgs e)
    {
        try
        {
            lblError.Visible = false;

            MakeConnection();
            SqlConn.Open();

            // Create SQL statement with parameter names, e.g. @parameter_name, 
            // were values should be inserted
            string SqlString = "insert into Job(JobType, Description, StartDate, Time, CompanyID, PositionFilled, JobStatus)" +
                "values(@JobType, @Description, @StartDate, @Time, @CompanyID, @PositionFilled, @JobStatus)";
            SqlCommand SqlCmd = new SqlCommand(SqlString, SqlConn);

            // Assign values to the parameters
            SqlCmd.Parameters.AddWithValue("@CompanyID", Convert.ToInt32(txtCompanyID.Text));
            SqlCmd.Parameters.AddWithValue("@JobType", txtJobType.Text);
            SqlCmd.Parameters.AddWithValue("@Description", txtDescript.Text);
            SqlCmd.Parameters.AddWithValue("@StartDate", cldStartDate.SelectedDate);
            SqlCmd.Parameters.AddWithValue("@Time", DateTime.Parse(txtTime.Text));
            SqlCmd.Parameters.AddWithValue("@PositionFilled", chkPosition.Checked?"1":"0");
            SqlCmd.Parameters.AddWithValue("@JobStatus", txtStatus.Text);

            SqlCmd.ExecuteNonQuery();

            lblError.Text = "Record updated successfully";
            lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
        finally
        {
            SqlConn.Close();  // Always close the connection
        }
    }
}
