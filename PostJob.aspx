﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PostJob.aspx.cs" Inherits="PostJob" Title="Post Job" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Post a new job&#39;s details</p>
    
    <table class="style1" cellpadding="5">
        <tr>
            <td>
                CompanyID</td>
            <td>
                <asp:TextBox ID="txtCompanyID" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Job ID</td>
            <td>
                <asp:TextBox ID="txtJobID" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Job Type</td>
            <td>
                <asp:TextBox ID="txtJobType" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Job Description</td>
            <td>
                <asp:TextBox ID="txtDescript" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Job Starting Date</td>
            <td align="center" style="width:50%">
                <asp:Calendar ID="cldStartDate" runat="server" Width="16px" ></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                Time</td>
            <td>
                <asp:TextBox ID="txtTime" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Position Filled</td>
            <td>
                <asp:CheckBox ID="chkPosition" runat="server" Text="Position Filled" />
            </td>
        </tr>
        <tr>
            <td>
                Job Status</td>
            <td>
                <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
            </td>
        </tr>
        </table>
    <asp:Button ID="btnPost" runat="server" onclick="btnPost_Click" 
        Text="Insert Job Details" />
    <br />
    <p>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:Image ID="imglgo" runat="server" Height="16px" 
            ImageUrl="~/assets/Images/KLogo.jpg" Width="16px" />
&nbsp;<asp:Label ID="lblK" runat="server" 
            Text="Kesundheit 2012 ™ NuTemps Ltd ® is a registered trademark"></asp:Label>
    </p>
</asp:Content>

