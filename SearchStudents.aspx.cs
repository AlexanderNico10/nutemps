﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Services;

public partial class SearchStudents : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["companyID"] == null)
        {
            Response.Redirect("CompanyLogin.aspx");
        }

        LinkButton lnkBtLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        lnkBtLogout.Visible = true;
    }
    protected void btnCourse_Click(object sender, EventArgs e)
    {
        try
        {
            lblError.Visible = false;
            txtSkill.Text = "";

            MakeConnection();

            string SqlString = "Select StudCode, FirstName, LastName, Telephone, Course, JobSkill, Experience from Student where Course like '%" +
                    txtCourse.Text+"%'";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Students");
            SqlDA.Fill(DS);

            // Check if any records were returned by the query
            if (DS.Tables[0].Rows.Count > 0)
            {
                // Display the returned record's values in the textboxes
                grdStudents.DataSource = DS;
                grdStudents.DataBind();        
            }
            else
            {
                grdStudents.DataSource = DS;
                grdStudents.DataBind(); 
                // If no data were returned (the record not found),
                // clear the textboxes and display message
                
                lblError.Text = "No record found.";
                lblError.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void btnSkill_Click(object sender, EventArgs e)
    {
        try
        {
            lblError.Visible = false;
            txtCourse.Text = "";

            MakeConnection();

            string SqlString = "Select StudCode, FirstName, LastName, Telephone, Course, JobSkill, Experience from Student where JobSkill like '%" +
                    txtSkill.Text +"%'";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Students");
            SqlDA.Fill(DS);

            // Check if any records were returned by the query
            if (DS.Tables[0].Rows.Count > 0)
            {
                // Display the returned record's values in the textboxes
                grdStudents.DataSource = DS;
                grdStudents.DataBind();
            }
            else
            {
                grdStudents.DataSource = DS;
                grdStudents.DataBind(); 
                // If no data were returned (the record not found),
                // clear the textboxes and display message

                lblError.Text = "No record found.";
                lblError.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
}