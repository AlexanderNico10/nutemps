﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Company : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["companyID"] == null)
        {
            Response.Redirect("CompanyLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;

        if (!IsPostBack)
        {
            try
            {
                // Check for postback - only retrieve the data on first
                // entering the page.
                MakeConnection();

                string SqlString = "Select * from Company";
                SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
                DataSet DS = new DataSet("Company");
                SqlDA.Fill(DS);

                SqlConn.Open();
                string Sql = "Select * from company where compname='" + Convert.ToString(Session["compname"]) + "'";
                SqlCommand SqlCmd = new SqlCommand(Sql, SqlConn);
                SqlDataReader sqlDR = SqlCmd.ExecuteReader();

                while (sqlDR.Read())
                {
                    txtCompID.Text = (sqlDR["companyid"].ToString());
                    txtCompName.Text = (sqlDR["compName"].ToString());
                    txtBusType.Text = (sqlDR["businesstype"].ToString());
                    txtContact.Text = (sqlDR["contactperson"].ToString());
                    txtTele.Text = (sqlDR["telephone"].ToString());
                    txtAddress.Text = (sqlDR["address"].ToString());
                    txtEmail.Text = (sqlDR["emailaddress"].ToString());
                    txtPass.Text = (sqlDR["pword"].ToString());
                }
                sqlDR.Close();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
            }
            finally
            {
                SqlConn.Close();
            }
        }
    }

    protected void btnPost_Click(object sender, EventArgs e)
    {
        try
        {
            lblError.Visible = false;

            MakeConnection();
            SqlConn.Open();

            // Create SQL statement with parameter names, e.g. @parameter_name, 
            // were values should be inserted
            SqlCommand SqlCmd = SqlConn.CreateCommand();
            
            SqlCmd.CommandText = "update Company set compname='"+txtCompName.Text+"', address='"+txtAddress.Text+"', " +
                    "Pword='"+txtPass.Text+"', BusinessType='"+txtBusType.Text+"', ContactPerson='"+txtContact.Text+"', " +
                    "EmailAddress='"+txtEmail.Text+"', Telephone='"+txtTele.Text+"' where CompanyID = '"+Convert.ToInt32(txtCompID.Text)+"'";
            SqlCmd.ExecuteNonQuery();

            SqlConn.Close();
            lblError.Text = "Record updated successfully";
            lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
        finally
        {
            SqlConn.Close();  // Always close the connection
        }
    }
}
