﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class CompanyMenu : System.Web.UI.Page
{
    SqlConnection SqlConn;
    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["companyID"] == null)
        {
            Response.Redirect("CompanyLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;

        try
        {
            lblError.Visible = false;
            lblLogged.Text = Convert.ToString(Session["CompName"]);
            MakeConnection();

            string SqlString = "Select * from company where compname = '" +
                    Session["CompName"] + "'";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Company");
            SqlDA.Fill(DS);

            // Connect the gridview to the DataSet
            grvJobs.DataSource = DS;
            grvJobs.DataBind();

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
}
