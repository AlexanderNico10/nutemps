﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ShowJobs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["companyID"] == null)
        {
            Response.Redirect("CompanyLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;
        
        if (!IsPostBack)
        {          
            try
            {
                lblJobsPosted.Text = "Jobs posted for " + Session["CompName"];

                GridViewData();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }
        }
    }
    public void GridViewData()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();
        SqlConnection conn = new SqlConnection(strConnectionString);
        conn.Open();
        SqlCommand com = new SqlCommand("select * from job where companyid='"+Convert.ToInt32(Session["companyid"])+"'", conn);
        com.ExecuteNonQuery();
        SqlDataAdapter adap = new SqlDataAdapter(com);
        DataSet ds = new DataSet();
        adap.Fill(ds);

        

        // Check if any records were returned by the query
        if (ds.Tables[0].Rows.Count > 0)
        {
            // Display the returned record's values in the textboxes
            grdJobs.DataSource = ds;
            grdJobs.DataBind();
        }
        else
        {
            // If no data were returned (the record not found),
            // clear the textboxes and display message

            lblError.Text = "No jobs posted.";
            lblError.Visible = true;
        }
    }

    protected void grdJobs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdJobs.EditIndex = -1;
        GridViewData();
    }
    protected void grdJobs_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int jobid = Convert.ToInt32(grdJobs.Rows[e.RowIndex].Cells[1].Text);

        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();
        SqlConnection conn = new SqlConnection(strConnectionString);
        conn.Open();
        SqlCommand com = new SqlCommand("delete from job where jobid='" + jobid + "'", conn);
        com.ExecuteNonQuery();
        GridViewData();
    }
    protected void grdJobs_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdJobs.EditIndex = e.NewEditIndex;
        GridViewData();
    }
    protected void grdJobs_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int jobid = Convert.ToInt32(((TextBox)grdJobs.Rows[e.RowIndex].Cells[1].Controls[0]).Text);
        string jobtype = ((TextBox)grdJobs.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
        string description = ((TextBox)grdJobs.Rows[e.RowIndex].Cells[3].Controls[0]).Text;
        DateTime startdate = Convert.ToDateTime(((TextBox)grdJobs.Rows[e.RowIndex].Cells[4].Controls[0]).Text);
        DateTime time = Convert.ToDateTime(((TextBox)grdJobs.Rows[e.RowIndex].Cells[5].Controls[0]).Text);
        int companyid = Convert.ToInt32(((TextBox)grdJobs.Rows[e.RowIndex].Cells[6].Controls[0]).Text);
        bool positionfilled = Convert.ToBoolean(((CheckBox)grdJobs.Rows[e.RowIndex].Cells[7].Controls[0]).Checked);
        string jobstatus = ((TextBox)grdJobs.Rows[e.RowIndex].Cells[8].Controls[0]).Text;

        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();
        SqlConnection conn = new SqlConnection(strConnectionString);
        conn.Open();
        SqlCommand com = new SqlCommand("update job set jobtype='" + jobtype + "',description='"+description+"',"+
            "startdate='"+startdate+"',time='"+time+"',companyid='"+companyid+"',positionfilled='"+positionfilled+"',"+
            "jobstatus='"+jobstatus+"' where jobid='"+jobid+"'", conn);
        com.ExecuteNonQuery();
        GridViewData();
    }
}