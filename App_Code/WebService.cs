﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]

public class WebService : System.Web.Services.WebService {

    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<string> GetJobs(string term) {
        
        List<string> listJobType = new List<string>();

        string cs = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(cs))
        {
            SqlCommand cmd = new SqlCommand("spGetJobs", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@term",
                Value = term
            });

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listJobType.Add(reader["JobType"].ToString());
            }
        }

        return listJobType;
    }

    [WebMethod]
    public List<string> GetjobsByDescription(string term)
    {
        List<string> listJobDescription = new List<string>();

        string connString = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        using (SqlConnection con = new SqlConnection(connString))
        {
            SqlCommand cmd = new SqlCommand("GetJobByDescription", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@term",
                Value = term
            });

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listJobDescription.Add(reader["Description"].ToString());
            }
        }

        return listJobDescription;
    }

    [WebMethod]
    public List<string> GetCourse(string term)
    {
        List<string> listCourse = new List<string>();

        string connString = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        using (SqlConnection con = new SqlConnection(connString)) 
        {
            SqlCommand cmd = new SqlCommand("GetCourse1", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@term",
                Value = term
            });

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listCourse.Add(reader["Course"].ToString());
            }
        }

        return listCourse;
    }

    [WebMethod]
    public List<string> GetSkill(string term)
    {
        List<string> listSkill = new List<string>();

        string connString = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        using (SqlConnection con = new SqlConnection(connString))
        {
            SqlCommand cmd = new SqlCommand("GetSkill", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@term",
                Value = term
            });

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listSkill.Add(reader["JobSkill"].ToString());
            }
        }

        return listSkill;
    }
}
