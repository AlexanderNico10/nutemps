﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StudentRegister.aspx.cs" Inherits="StudentRegister" Title="StudentRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Register your student details<br />
    <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Label" 
        Visible="False"></asp:Label>
    <br />
<table class="style1">
    <tr>
        <td>
            <asp:Label ID="lblStudCode" runat="server" Text="Student Code"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblFirst" runat="server" Text="First Name"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblLast" runat="server" Text="Last Name"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblTel" runat="server" Text="Telephone Number"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblCourse" runat="server" Text="Course"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtCourse" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblPword" runat="server" Text="Password"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblSkill" runat="server" Text="Job Skill"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtSkill" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblExperience" runat="server" Text="Experience acquired"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtExperience" runat="server"></asp:TextBox>
        </td>
    </tr>
    </table>
<asp:Button ID="btnRegister" runat="server" Text="Register" 
    onclick="btnRegister_Click" />
<br />
</asp:Content>

