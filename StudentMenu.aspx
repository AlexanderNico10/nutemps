﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StudentMenu.aspx.cs" Inherits="StudentMenu" Title="Student Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style5
        {
            width: 445px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblName" runat="server"></asp:Label>
    <br />
    <br />
    <table class="style1" cellpadding="5">
        <tr>
            <td style="text-align: center" class="style5">
                <asp:HyperLink ID="hypSearch" runat="server" 
                    NavigateUrl="~/SearchCurrentJobs.aspx">Search Current Jobs</asp:HyperLink>
            </td>
            <td style="text-align: center">
                <asp:HyperLink ID="hypeChange" runat="server" 
                    NavigateUrl="~/StudentEditRegister.aspx">Change My Details</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" class="style5">
                <asp:Image ID="imgSearch" runat="server" 
                    ImageUrl="~/assets/Images/2b2db11094e6743cbd427ed9cd553746x.jpg.cf.png" Height="200px" 
                    Width="350px" />
            </td>
            <td style="text-align: center">
                <asp:Image ID="imgGears" runat="server" 
                    ImageUrl="~/assets/Images/Gears_Transparent.png.cf.png" Height="200px" Width="350px" />
            </td>
        </tr>
    </table>
    <br />
    <p>
        <asp:Button ID="btnRemove" runat="server" onclick="btnRemove_Click" 
            Text="Delete Student Profile" />
&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblDone" runat="server" Text="Details removed" Visible="False"></asp:Label>
    </p>
</asp:Content>

