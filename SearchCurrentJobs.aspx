﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SearchCurrentJobs.aspx.cs" Inherits="SearchCurrentJobs" Title="Search Current Jobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="assets/js/jquery-1.11.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_txtType').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: 'WebService.asmx/GetJobs',
                        method: 'post',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ term: request.term }),
                        dataType: 'json',
                        success: function (data) {
                            response(data.d)
                        },
                        error: function (err) {
                            alert(err);
                        }
                    });
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_txtDescript').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: 'WebService.asmx/GetjobsByDescription',
                        method: 'post',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ term: request.term }),
                        dataType: 'json',
                        success: function (data) {
                            response(data.d)
                        },
                        error: function (err) {
                            alert(err);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <p>
        Search Current Jobs</p>
    
    <asp:Panel ID="pnlType" runat="server" DefaultButton="btnType">
    <table class="style1">
        <tr>
            <td style="width:33%">
                Search according to Type</td>
            <td style="width:33%">
                <asp:TextBox ID="txtType" runat="server"></asp:TextBox>
            </td>
            <td style="width:33%">
                <asp:Button ID="btnType" runat="server" onclick="btnType_Click" 
                    Text="Search Job Types" Width="160px"/>
            </td>
        </tr>
    </table>
    </asp:Panel>
    
    <asp:Panel ID="pnlDesc" runat="server" DefaultButton="btnDescription">
    <table class="style1">
       <tr>
            <td style="width:33%">
                Search according to Description</td>
            <td style="width:33%">
                <asp:TextBox ID="txtDescript" runat="server"></asp:TextBox>
            </td>
            <td style="width:33%">
                <asp:Button ID="btnDescription" runat="server" onclick="btnDescription_Click" 
                    Text="Search Job Descriptions" Width="160px"/>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <br />
    <asp:GridView ID="grdJobs" runat="server" GridLines="None" 
        HorizontalAlign="Center" CellPadding="4" EnableModelValidation="True" 
        ForeColor="#333333">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    <br />
    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
    <br />
    </asp:Content>

