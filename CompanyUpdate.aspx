﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CompanyUpdate.aspx.cs" Inherits="Company" Title="Update Company Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Update company details</p>
    <p>
        &nbsp;</p>
    <table class="style1" cellpadding="5">
        <tr>
            <td>
                Company ID</td>
            <td>
                <asp:TextBox ID="txtCompID" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Company Name</td>
            <td>
                <asp:TextBox ID="txtCompName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Password</td>
            <td>
                <asp:TextBox ID="txtPass" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                BusinessType</td>
            <td>
                <asp:TextBox ID="txtBusType" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Contact Person</td>
            <td>
                <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Telephone</td>
            <td>
                <asp:TextBox ID="txtTele" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Address</td>
            <td>
                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Email Address</td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnPost" runat="server" onclick="btnPost_Click" 
        Text="Update Company Details" />
    <br />
    <br />
    <asp:Label ID="lblError" runat="server"></asp:Label>
</asp:Content>

