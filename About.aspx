﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>NuTemps is an e-commerce website that serves as a bridge between employers and students looking for employment.</p>
    <p>Students can register a profile using their details  
        and subsequently view vacant positions. Companies can register an employer 
        profile using the company&#39;s details and post positions that are on offer.</p>
    <p>
        <asp:Image ID="imgHead" runat="server" 
            ImageUrl="~/assets/Images/buildings-offices.jpg" />
    </p>
<p>
        <asp:Image ID="imgK" runat="server" Height="16px" 
            ImageUrl="~/assets/Images/KLogo.jpg" />
&nbsp;<asp:Label ID="lblFoot" runat="server" 
            Text="Kesundheit 2012 ™ NuTemps Ltd ® is a registered trademark"></asp:Label>
    </p>
</asp:Content>

