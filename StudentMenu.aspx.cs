﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class StudentMenu : System.Web.UI.Page
{
    SqlConnection SqlConn;

    void MakeConnection()
    {
        // Either directly specify connection string as below, or
        // retrieve connection string from web.config
        string strConnectionString = ConfigurationManager.ConnectionStrings["database"].ToString();

        // Only create new connection if no connection has been made yet
        if (SqlConn == null)
        {
            SqlConn = new SqlConnection(strConnectionString);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["studcode"] == null)
        {
            Response.Redirect("StudentLogin.aspx");
        }

        LinkButton btnLogout = Master.FindControl("lnkBtLogout") as LinkButton;
        btnLogout.Visible = true;

        // Check for postback - only retrieve the data on first
        // entering the page.
        if (!Page.IsPostBack)
        {
            try
            {
                MakeConnection();

                lblName.Text = "Hello " + Convert.ToString(Session["name"]);

                string SqlUser = "SELECT * From Student";
                SqlDataAdapter SqlDA = new SqlDataAdapter(SqlUser, SqlConn);
                DataSet DS = new DataSet("Student");
                SqlDA.Fill(DS);
            }
            catch (Exception ex)
            {
                lblDone.Text = ex.ToString();
            }
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        try
        {
            MakeConnection();

            string SqlString = "delete * from Student where studcode='" +
                    Convert.ToString(Session["studcode"]) + "'";
            SqlDataAdapter SqlDA = new SqlDataAdapter(SqlString, SqlConn);
            DataSet DS = new DataSet("Students");
            SqlDA.Fill(DS);
            lblDone.Visible = true;
            Session.Abandon();
            Response.Redirect("Home.aspx");
        }
        catch (Exception ex)
        {
            lblDone.Text = ex.ToString();
        }
    }
}
